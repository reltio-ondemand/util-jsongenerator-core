package com.reltio.etl.service.domain;

import static com.reltio.etl.constants.JsonGeneratorProperties.JSON_WITHOUT_CROSSWALK;
import static com.reltio.etl.constants.JsonGeneratorProperties.MAX_THREAD_COUNT;
import static com.reltio.etl.constants.JsonGeneratorProperties.MIN_THREAD_COUNT;
import static com.reltio.etl.constants.JsonGeneratorProperties.SINGLE_JSON_OUTPUT;

import java.io.Serializable;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import com.reltio.etl.domain.RefSystem;
import com.reltio.etl.service.util.JSONGeneratorUtil;
import com.reltio.etl.util.NormalizedJsonConversationHelper;

public class EntityConfigurationProperties implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4437705409844325789L;

	private String inputDataFilePath;
	private String inputFileFormat;
	private String inputFileMask;
	private String inputFileDelimiter;
	private String outputFilePath;
	private String mappingFilePath;
	private String entityType;
	private Integer threadCount;
	private String columnDelimiter;
	private String nestedDelimiter;
	private String awsKey;
	private String awsSecretKey;
	private String awsRegion;
	private Boolean isSingleJsonOutput = false;
	private Boolean isJsonWithOutCrosswalk = false;

	private String crosswalkCreateDate;
	private String crosswalkUpdateDate;
	private String crosswalkDeleteDate;

	private RefSystem refSystem;
	private String refAttributeSourceSystem;

	private CrosswalkProperty dataProviderCrosswalk;

	private List<CrosswalkProperty> nonDPCrosswalks;
	
	private boolean ignoreNullAttribute=true;

	public EntityConfigurationProperties(Properties properties) {

		// READ the Config Properties values
		inputDataFilePath = properties.getProperty("INPUT_DATA_FILE");
		inputFileFormat = properties.getProperty("INPUT_FILE_FORMAT");
		inputFileMask = properties.getProperty("INPUT_FILE_MASK");
		inputFileDelimiter = properties.getProperty("INPUT_FILE_DELIMITER");
        outputFilePath = properties.getProperty("OUTPUT_FILE");
		mappingFilePath = properties.getProperty("MAPPING_FILE");

		entityType = properties.getProperty("ENTITY_TYPE");
		String sourceSystem = properties.getProperty("SOURCE_SYSTEM");
		String sourceTable = properties.getProperty("SOURCE_TABLE");
		
		awsKey = properties.getProperty("AWS_KEY");
		awsSecretKey = properties.getProperty("AWS_SECRET_KEY");
		awsRegion = properties.getProperty("AWS_REGION");
		
		refSystem = new RefSystem();
		
        String refAttributeSourceSystem = properties
                .getProperty("REFERENCE_ATTRIBUTE_SOURCE_SYSTEM");
		String crosswalkValueColumn = properties.getProperty("CROSSWALK_VALUE_COLUMN");

		dataProviderCrosswalk = JSONGeneratorUtil.createCrosswalkProperty(sourceSystem, crosswalkValueColumn, sourceTable);

		if (refAttributeSourceSystem == null) {
			refAttributeSourceSystem = dataProviderCrosswalk.getSourceSystem();
		} else {
			if(NormalizedJsonConversationHelper.isStaticValue(refAttributeSourceSystem)) {
				refAttributeSourceSystem = JSONGeneratorUtil.getStaticSourceSystem(refAttributeSourceSystem);
			}
		}
		
		refSystem.setRefEntitySourceSystem(refAttributeSourceSystem);
		
		
        String refAttributeSourceTable = properties
                .getProperty("REFERENCE_ATTRIBUTE_SOURCE_TABLE");
        refSystem.setRefEntitySourceTable(refAttributeSourceTable);

        String refRelationSourceTable = properties
                .getProperty("REFERENCE_RELATION_SOURCE_TABLE");
        refSystem.setRefRelationSourceTable(refRelationSourceTable);

        
        
        String refRelationSourceSystem = properties
                .getProperty("REFERENCE_RELATION_SOURCE_SYSTEM");

        if(refRelationSourceSystem!= null && NormalizedJsonConversationHelper.isStaticValue(refRelationSourceSystem)) {
            refSystem.setRefRelationSourceSystem(JSONGeneratorUtil.getStaticSourceSystem(refRelationSourceSystem));
        }else {
            refSystem.setRefRelationSourceSystem(refRelationSourceSystem);
        }

		
		nonDPCrosswalks = JSONGeneratorUtil.createdNonDPCrosswalks(properties);

		columnDelimiter = properties
				.getProperty("NORMALIZED_FILE_COLUMN_DELIMITER");

		if (columnDelimiter != null && !columnDelimiter.isEmpty()) {
			columnDelimiter = Pattern.quote(columnDelimiter);
		}
		
		nestedDelimiter = properties
				.getProperty("NESTED_NORMALIZED_FILE_COLUMN_DELIMITER");


		if (nestedDelimiter != null && !nestedDelimiter.isEmpty()) {
			nestedDelimiter = Pattern.quote(nestedDelimiter);
		}

		String threadCountStr = properties.getProperty("THREAD_COUNT");
		if (threadCountStr == null || threadCountStr.isEmpty()) {
			setThreadCount(null);
		} else {
			setThreadCount(Integer.parseInt(threadCountStr));
		}

		String jsonFormat = properties.getProperty("JSON_OUTPUT_FORMAT");
		if (jsonFormat != null) {
			if (jsonFormat.equalsIgnoreCase(SINGLE_JSON_OUTPUT)) {
				isSingleJsonOutput = true;

			} else if (jsonFormat.equalsIgnoreCase(JSON_WITHOUT_CROSSWALK)) {
				isJsonWithOutCrosswalk = true;

			}
		}

		crosswalkCreateDate = properties
				.getProperty("CROSSWALK_CREATE_DATE_COLUMN");

		crosswalkUpdateDate = properties
				.getProperty("CROSSWALK_UPDATE_DATE_COLUMN");

		crosswalkDeleteDate = properties
				.getProperty("CROSSWALK_DELETE_DATE_COLUMN");
		ignoreNullAttribute=Boolean.valueOf(properties.getProperty("GENERATE_NULL_VALUES"));


	}

	
	public boolean isIgnoreNullAttribute() {
		return ignoreNullAttribute;
	}


	public void setIgnoreNullAttribute(boolean ignoreNullAttribute) {
		this.ignoreNullAttribute = ignoreNullAttribute;
	}


	/**
	 * @return the inputDataFilePath
	 */
	public String getInputDataFilePath() {
		return inputDataFilePath;
	}

	/**
	 * @param inputDataFilePath
	 *            the inputDataFilePath to set
	 */
	public void setInputDataFilePath(String inputDataFilePath) {
		this.inputDataFilePath = inputDataFilePath;
	}
	
	/**
	 * @return the inputFileFormat
	 */
	public String getInputFileFormat() {
		return inputFileFormat;
	}

	/**
	 * @param inputFileFormat
	 *            the inputFileFormat to set
	 */
	public void setInputFileFormat(String inputFileFormat) {
		this.inputFileFormat = inputFileFormat;
	}

	/**
	 * @return the inputFileMask
	 */
	public String getInputFileMask() {
		return inputFileMask;
	}


	/**
	 * @param inputFileMask the inputFileMask to set
	 */
	public void setInputFileMask(String inputFileMask) {
		this.inputFileMask = inputFileMask;
	}


	/**
	 * @return the inputFileDelimiter
	 */
	public String getInputFileDelimiter() {
		return inputFileDelimiter;
	}

	/**
	 * @param inputFileDelimiter
	 *            the inputFileDelimiter to set
	 */
	public void setInputFileDelimiter(String inputFileDelimiter) {
		this.inputFileDelimiter = inputFileDelimiter;
	}

	/**
	 * @return the outputFilePath
	 */
	public String getOutputFilePath() {
		return outputFilePath;
	}

	/**
	 * @param outputFilePath
	 *            the outputFilePath to set
	 */
	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}

	/**
	 * @return the mappingFilePath
	 */
	public String getMappingFilePath() {
		return mappingFilePath;
	}

	/**
	 * @param mappingFilePath
	 *            the mappingFilePath to set
	 */
	public void setMappingFilePath(String mappingFilePath) {
		this.mappingFilePath = mappingFilePath;
	}

	/**
	 * @return the entityType
	 */
	public String getEntityType() {
		return entityType;
	}

	/**
	 * @param entityType
	 *            the entityType to set
	 */
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	/**
	 * @return the threadCount
	 */
	public Integer getThreadCount() {
		return threadCount;
	}

	/**
	 * @param threadCount
	 *            the threadCount to set
	 */
	public void setThreadCount(Integer threadCount) {
		if (threadCount == null) {
			this.threadCount = MIN_THREAD_COUNT;
		} else if (threadCount > MAX_THREAD_COUNT) {
			this.threadCount = MAX_THREAD_COUNT;

		} else {
			this.threadCount = threadCount;
		}
	}

	/**
	 * @return the columnDelimiter
	 */
	public String getColumnDelimiter() {
		return columnDelimiter;
	}

	/**
	 * @param columnDelimiter
	 *            the columnDelimiter to set
	 */
	public void setColumnDelimiter(String columnDelimiter) {
		this.columnDelimiter = columnDelimiter;
	}

	/**
	 * @return the isSingleJsonOutput
	 */
	public Boolean getIsSingleJsonOutput() {
		return isSingleJsonOutput;
	}

	/**
	 * @param isSingleJsonOutput
	 *            the isSingleJsonOutput to set
	 */
	public void setIsSingleJsonOutput(Boolean isSingleJsonOutput) {
		this.isSingleJsonOutput = isSingleJsonOutput;
	}

	/**
	 * @return the isJsonWithOutCrosswalk
	 */
	public Boolean getIsJsonWithOutCrosswalk() {
		return isJsonWithOutCrosswalk;
	}

	/**
	 * @param isJsonWithOutCrosswalk
	 *            the isJsonWithOutCrosswalk to set
	 */
	public void setIsJsonWithOutCrosswalk(Boolean isJsonWithOutCrosswalk) {
		this.isJsonWithOutCrosswalk = isJsonWithOutCrosswalk;
	}

	/**
	 * @return the crosswalkCreateDate
	 */
	public String getCrosswalkCreateDate() {
		return crosswalkCreateDate;
	}

	/**
	 * @param crosswalkCreateDate
	 *            the crosswalkCreateDate to set
	 */
	public void setCrosswalkCreateDate(String crosswalkCreateDate) {
		this.crosswalkCreateDate = crosswalkCreateDate;
	}

	/**
	 * @return the crosswalkUpdateDate
	 */
	public String getCrosswalkUpdateDate() {
		return crosswalkUpdateDate;
	}

	/**
	 * @param crosswalkUpdateDate
	 *            the crosswalkUpdateDate to set
	 */
	public void setCrosswalkUpdateDate(String crosswalkUpdateDate) {
		this.crosswalkUpdateDate = crosswalkUpdateDate;
	}

	/**
	 * @return the crosswalkDeleteDate
	 */
	public String getCrosswalkDeleteDate() {
		return crosswalkDeleteDate;
	}

	/**
	 * @param crosswalkDeleteDate
	 *            the crosswalkDeleteDate to set
	 */
	public void setCrosswalkDeleteDate(String crosswalkDeleteDate) {
		this.crosswalkDeleteDate = crosswalkDeleteDate;
	}

	/**
	 * @return the refAttributeSourceSystem
	 */
	public String getRefAttributeSourceSystem() {
		return refAttributeSourceSystem;
	}

	/**
	 * @param refAttributeSourceSystem
	 *            the refAttributeSourceSystem to set
	 */
	public void setRefAttributeSourceSystem(String refAttributeSourceSystem) {
		this.refAttributeSourceSystem = refAttributeSourceSystem;
	}

	public CrosswalkProperty getDataProviderCrosswalk() {
		return dataProviderCrosswalk;
	}

	public List<CrosswalkProperty> getNonDPCrosswalks() {
		return nonDPCrosswalks;
	}


	/**
	 * @return the nestedDelimiter
	 */
	public String getNestedDelimiter() {
		return nestedDelimiter;
	}


	/**
	 * @param nestedDelimiter the nestedDelimiter to set
	 */
	public void setNestedDelimiter(String nestedDelimiter) {
		this.nestedDelimiter = nestedDelimiter;
	}


	/**
	 * @return the refSystem
	 */
	public RefSystem getRefSystem() {
		return refSystem;
	}


	/**
	 * @param refSystem the refSystem to set
	 */
	public void setRefSystem(RefSystem refSystem) {
		this.refSystem = refSystem;
	}
	/**
	 * @return the awsKey
	 */
	public String getAwsKey() {
		return awsKey;
	}


	/**
	 * @param awsKey the awsKey to set
	 */
	public void setAwsKey(String awsKey) {
		this.awsKey = awsKey;
	}


	/**
	 * @return the awsSecretKey
	 */
	public String getAwsSecretKey() {
		return awsSecretKey;
	}


	/**
	 * @param awsSecretKey the awsSecretKey to set
	 */
	public void setAwsSecretKey(String awsSecretKey) {
		this.awsSecretKey = awsSecretKey;
	}


	/**
	 * @return the awsRegion
	 */
	public String getAwsRegion() {
		return awsRegion;
	}


	/**
	 * @param awsRegion the awsRegion to set
	 */
	public void setAwsRegion(String awsRegion) {
		this.awsRegion = awsRegion;
	}

}
