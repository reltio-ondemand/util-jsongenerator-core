package com.reltio.etl.service;

import com.google.gson.Gson;
import com.reltio.cst.util.Util;
import com.reltio.etl.constants.JsonGeneratorProperties;
import com.reltio.etl.domain.Crosswalk;
import com.reltio.etl.domain.ReltioObject;
import com.reltio.etl.service.domain.RelationConfigurationProperties;
import com.reltio.etl.service.util.JSONGeneratorUtil;
import com.reltio.etl.util.NormalizedJsonConversationHelper;
import com.reltio.file.*;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import static com.reltio.etl.constants.JsonGeneratorProperties.GROUP_TO_SENT;
import static com.reltio.etl.constants.JsonGeneratorProperties.MAX_QUEUE_SIZE_MULTIPLICATOR;
import static com.reltio.etl.service.util.JSONGeneratorUtil.waitForTasksReady;

/**
 * This is the main class for generating json for Relations. Handles both
 * Normalized and denormalized file columns
 * 
 *
 */
public class HasOPSIRelationJsonGeneratorService {

	// Keep column name and index in the input file
	private final Map<String, Integer> columnIndexMap = new HashMap<>();

	// Keep non-valid column details list
	private final Set<String> nonColumnValues = new HashSet<>();

	// Keeps Normalized Value columns
	private final Set<String> normalizedColumns = new HashSet<>();

	// keeps the Source System of the File
	private String fileSourceSystem = null;

	private static Gson gson = new Gson();

	private final List<ReltioObject> reltioOjectsOfFile = new ArrayList<>();

	@SuppressWarnings({ "unchecked", "rawtypes" })
    public static void main(String[] args) throws Exception {
		String firstLine = "100000000~~184955839~~JP~~700514920~~YOKOYAMAEN~~null~~よこやま園~~null";

		System.out.println("Process Started");
        ReltioFileWriter tempReltioFileWriter = null;
		long programStartTime = System.currentTimeMillis();
		int count = 0;
		final HasOPSIRelationJsonGeneratorService jsonGeneratorService = new HasOPSIRelationJsonGeneratorService();
		final int[] rejectedRecordCount = new int[1];
		rejectedRecordCount[0] = 0;

		Properties properties = new Properties();
		FileReader fileReader = null;
		try {
			String propertyFilePath = args[0];
			fileReader = new FileReader(
					StringEscapeUtils.escapeJava(propertyFilePath));
			//properties.load(fileReader);
			properties = Util.getProperties(propertyFilePath, "AWS_KEY", "AWS_SECRET_KEY");
		} catch (Exception e) {
			System.out.println("Failed Read the Properties File :: ");
			e.printStackTrace();
		} finally {
			if (fileReader != null) {
				try {
					fileReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		// READ the Config Properties values
		final RelationConfigurationProperties configProperties = new RelationConfigurationProperties(
				properties);

		if (args != null && args.length > 1) {
			configProperties.setInputDataFilePath(args[1]);
			configProperties.setMappingFilePath(args[2]);
			configProperties.setOutputFilePath(args[3]);
		}

		if (configProperties.getInputDataFilePath() == null
				|| configProperties.getRelationType() == null
				|| configProperties.getInputFileFormat() == null
				|| configProperties.getOutputFilePath() == null
				|| configProperties.getSourceSystem() == null
				|| (configProperties.getEndObjCrosswalkValueColumn() == null && configProperties
						.getEndObjEntityUriColumn() == null)
				|| (configProperties.getStartObjCrosswalkValueColumn() == null && configProperties
						.getStartObjEntityUriColumn() == null)) {
			System.out
					.println("Error::: one or more required parameters missing. Please verify the input properties File....");
			System.exit(0);
		}
		jsonGeneratorService.fileSourceSystem = configProperties
				.getSourceSystem();

		// Read the Mapping File configuration
		properties.clear();
		if (configProperties.getMappingFilePath() != null
				&& !configProperties.getMappingFilePath().isEmpty()) {
			try {
				fileReader = new FileReader(
						StringEscapeUtils.escapeJava(configProperties
								.getMappingFilePath()));
				properties.load(fileReader);
			} catch (Exception e) {
				System.out.println("Failed to Read the Properties File :: "
						+ configProperties.getMappingFilePath());
				e.printStackTrace();
				System.exit(-1);
			} finally {
				if (fileReader != null) {
					try {
						fileReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		// Create Reader for input data File
		ReltioFileReader inputDataFileReader = null;
		final ReltioFileWriter failedRecordsWriter;
		String failedRecordsFileName;

		if (configProperties.getInputDataFilePath().substring(0, 2).equalsIgnoreCase("S3")) {
			String awsKey  = configProperties.getAwsKey();
			String awsSecretKey  = configProperties.getAwsSecretKey();
			String awsRegion = configProperties.getAwsRegion();
			String bucketName = configProperties.getInputDataFilePath().substring(5, StringUtils.ordinalIndexOf(StringEscapeUtils.escapeJava(configProperties.getInputDataFilePath()), "/", 3));
			String fileName = configProperties.getInputDataFilePath().substring(StringUtils.ordinalIndexOf(StringEscapeUtils.escapeJava(configProperties.getInputDataFilePath()), "/", 3)+1);
		if (configProperties.getInputFileFormat().equals("CSV")) {
			inputDataFileReader = new ReltioS3CSVFileReader(awsKey, awsSecretKey, awsRegion, bucketName, fileName);
		} else {
			if (configProperties.getInputFileDelimiter() == null) {
				inputDataFileReader = new ReltioS3FlatFileReader(awsKey, awsSecretKey, awsRegion, bucketName, fileName);
			} else {
				inputDataFileReader = new ReltioS3FlatFileReader(awsKey, awsSecretKey, awsRegion, bucketName, fileName,
						configProperties.getInputFileDelimiter());	
			}
		}
		} else{
			if (configProperties.getInputFileFormat().equals("CSV")) {
				inputDataFileReader = new ReltioCSVFileReader(configProperties.getInputDataFilePath());
			} else {
				if (configProperties.getInputFileDelimiter() == null) {
					inputDataFileReader = new ReltioFlatFileReader(configProperties.getInputDataFilePath());
				} else {
					inputDataFileReader = new ReltioFlatFileReader(configProperties.getInputDataFilePath(),
							configProperties.getInputFileDelimiter());	
				}
			}
		}
        if (configProperties.getOutputFilePath().substring(0, 2).equalsIgnoreCase("S3")) {
			String awsKey  = configProperties.getAwsKey();
			String awsSecretKey  = configProperties.getAwsSecretKey();
			String awsRegion = configProperties.getAwsRegion();
			String outBucketName = configProperties.getOutputFilePath().substring(5, StringUtils.ordinalIndexOf(StringEscapeUtils.escapeJava(configProperties.getOutputFilePath()), "/", 3));
			String outFileName = configProperties.getOutputFilePath().substring(StringUtils.ordinalIndexOf(StringEscapeUtils.escapeJava(configProperties.getOutputFilePath()), "/", 3)+1);
			tempReltioFileWriter = new ReltioS3FlatFileWriter(outBucketName, outFileName, awsKey, awsSecretKey, awsRegion);
			if (configProperties.getInputFileFormat().equals("CSV")) {
				failedRecordsFileName = outFileName + "_RejectedRecords.csv";
				failedRecordsWriter = new ReltioS3CSVFileWriter(outBucketName, failedRecordsFileName, awsKey, awsSecretKey, awsRegion);	
			}else {
				failedRecordsFileName = outFileName + "_RejectedRecords.txt";
				if (configProperties.getInputFileDelimiter() == null) {
					failedRecordsWriter = new ReltioS3FlatFileWriter(outBucketName, failedRecordsFileName, awsKey, awsSecretKey, awsRegion);
				} else {
					failedRecordsWriter = new ReltioS3FlatFileWriter(outBucketName, failedRecordsFileName, awsKey, awsSecretKey, awsRegion, "UTF-8",
							configProperties.getInputFileDelimiter());	
				}
			}
		} else {
			tempReltioFileWriter = new ReltioFlatFileWriter(configProperties.getOutputFilePath());
			if (configProperties.getInputFileFormat().equals("CSV")) {
				failedRecordsFileName = configProperties.getOutputFilePath() + "_RejectedRecords.csv";
				failedRecordsWriter = new ReltioCSVFileWriter(failedRecordsFileName);
			}else {
				failedRecordsFileName = configProperties.getOutputFilePath() + "_RejectedRecords.txt";
				if (configProperties.getInputFileDelimiter() == null) {
					failedRecordsWriter = new ReltioFlatFileWriter(failedRecordsFileName);
				} else {
					failedRecordsWriter = new ReltioFlatFileWriter(failedRecordsFileName, "UTF-8",
							configProperties.getInputFileDelimiter());	
				}
			}
		}
        
        // Create Writter for outputFile
        final ReltioFileWriter reltioFileWriter = tempReltioFileWriter;

		String[] lineValues = null;

		// Read Header
		//lineValues = inputDataFileReader.readLine();
		inputDataFileReader.readLine();
		lineValues = "OPSI_ROWID_ORG~~OPSI_ROWID_OBJECT~~ORG_CTRY_CD~~ORG_ORG_DUNS_UID~~ORG_XTND_NM~~ORG_STD_LGL_XTND_NM~~ONL_XTND_NM~~ORG_RPTG_NM".split("~~", -1);
		
		failedRecordsWriter.writeToFile(JSONGeneratorUtil
				.addValueInArrayBegining(
						JsonGeneratorProperties.FAILED_RECORD_REASON,
						lineValues));

		final int sizeOfHeaderColumn = lineValues.length;

		List<String> allColumnNames = Arrays.asList(lineValues);

		// Create a Map for Column Name to Index
		for (int i = 0; i < allColumnNames.size(); i++) {
			jsonGeneratorService.columnIndexMap.put(allColumnNames.get(i), i);
		}

		final Map<String, String> attributeMappingDetails = new HashMap<>(
				(Map) properties);

		// Populate all the attributes from mapping file to Reltio Dummy Object
		final Map<String, Object> attributesTemplateList;
		if (configProperties.getMappingFilePath() != null) {
			// Populate all the attributes from mapping file to Reltio Dummy
			// Object
			attributesTemplateList = JsonGeneratorService
					.createReltioTemplateObjectAttributesList(
							attributeMappingDetails,
							jsonGeneratorService.columnIndexMap,
							jsonGeneratorService.nonColumnValues,
							null,
							jsonGeneratorService.normalizedColumns);
			if ((configProperties.getColumnDelimiter() == null || configProperties
					.getColumnDelimiter().isEmpty())
					&& !jsonGeneratorService.normalizedColumns.isEmpty()) {
				System.out
						.println("Below File columns provided in the mapping File mentioned as Normalized Columns. But the NORMALIZED_FILE_COLUMN_DELIMITER property is empty..");

				for (String column : jsonGeneratorService.normalizedColumns) {
					System.out.println(column);
				}

				System.out
						.println("Please update the Job Configuration file with the NORMALIZED_FILE_COLUMN_DELIMITER value and Try again...");
				System.exit(-1);

			}

			// Check the crosswalk Column
			if (configProperties.getCrosswalkValueColumn() != null
					&& !configProperties.getCrosswalkValueColumn().isEmpty()
					&& jsonGeneratorService.columnIndexMap.get(configProperties
							.getCrosswalkValueColumn()) == null) {
				jsonGeneratorService.nonColumnValues.add(configProperties
						.getCrosswalkValueColumn());
			}

			boolean isNonColumnValuePresent = false;
			if (configProperties.getStartObjCrosswalkValueColumn() != null
					&& jsonGeneratorService.columnIndexMap.get(configProperties
							.getStartObjCrosswalkValueColumn()) == null) {
				jsonGeneratorService.nonColumnValues.add(configProperties
						.getStartObjCrosswalkValueColumn());
			}

			if (configProperties.getEndObjCrosswalkValueColumn() != null
					&& jsonGeneratorService.columnIndexMap.get(configProperties
							.getEndObjCrosswalkValueColumn()) == null) {
				jsonGeneratorService.nonColumnValues.add(configProperties
						.getEndObjCrosswalkValueColumn());
			}

			if (!jsonGeneratorService.nonColumnValues.isEmpty()) {

				for (String col : jsonGeneratorService.nonColumnValues) {
					if (!col.startsWith("{") || !col.endsWith("}")) {
						if (!isNonColumnValuePresent) {
							System.out
									.println("Non-Column Values in the Mapping File Below::::");
						}
						System.out.println(col);
						isNonColumnValuePresent = true;
					}
				}
			}

			if (isNonColumnValuePresent) {
				System.out
						.println("Please validate the Mapping File Column Names listed above and re-try.....");
				System.exit(-1);
			}

		} else {
			attributesTemplateList = null;
		}

		// Start and End Object crosswalk//Entity URI columns
		final Integer startObjectCrosswalkIndex, startObjectEntityUriIndex, endObjecCrosswalkIndex, endObjectEntityUriIndex;

		if (configProperties.getStartObjCrosswalkValueColumn() != null) {
			startObjectCrosswalkIndex = jsonGeneratorService.columnIndexMap
					.get(configProperties.getStartObjCrosswalkValueColumn());
			startObjectEntityUriIndex = null;
		} else {
			startObjectEntityUriIndex = jsonGeneratorService.columnIndexMap
					.get(configProperties.getStartObjEntityUriColumn());
			startObjectCrosswalkIndex = null;
		}

		if (configProperties.getEndObjCrosswalkValueColumn() != null) {
			endObjecCrosswalkIndex = jsonGeneratorService.columnIndexMap
					.get(configProperties.getEndObjCrosswalkValueColumn());
			endObjectEntityUriIndex = null;
		} else {
			endObjectEntityUriIndex = jsonGeneratorService.columnIndexMap
					.get(configProperties.getEndObjEntityUriColumn());
			endObjecCrosswalkIndex = null;
		}

		// Thread Operations
		ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors
				.newFixedThreadPool(configProperties.getThreadCount());
		ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();
		boolean eof = false;

		// Create Reltio Dummy Object
		final Crosswalk crosswalk = new Crosswalk();
		crosswalk.type = configProperties.getSourceSystem();
		List<String[]> inputLineValues = new ArrayList<>();

		final Integer crosswalkValueColummnIndex;
		if (configProperties.getCrosswalkValueColumn() != null
				&& !configProperties.getCrosswalkValueColumn().isEmpty()) {
			crosswalkValueColummnIndex = jsonGeneratorService.columnIndexMap
					.get(configProperties.getCrosswalkValueColumn());
		} else {
			crosswalkValueColummnIndex = null;
		}

		final Integer startDateIndex = jsonGeneratorService.columnIndexMap
				.get(configProperties.getStartDateColumn());
		final Integer endDateIndex = jsonGeneratorService.columnIndexMap
				.get(configProperties.getEndDateColumn());

		while (!eof) {
			for (int threadNum = 0; threadNum < configProperties
					.getThreadCount() * MAX_QUEUE_SIZE_MULTIPLICATOR; threadNum++) {
				inputLineValues.clear();
				if(count ==0) {
					System.out.println("Onetime");
					inputLineValues.add(firstLine.split("~~", -1));
				}
				for (int k = 0; k < GROUP_TO_SENT; k++) {

					// Read line
					lineValues = inputDataFileReader.readLine();
					if (lineValues == null) {
						eof = true;
						break;
					}
					count++;
					inputLineValues.add(lineValues);

				}

				if (!inputLineValues.isEmpty()) {
					final List<String[]> threadInputLines = new ArrayList<>(
							inputLineValues);

					futures.add(executorService.submit(new Callable<Long>() {

						@Override
						public Long call() {
							long requestExecutionTime = 0l;
							long startTime = System.currentTimeMillis();
							try {
								ReltioObject reltioObject;
								Crosswalk reltioObjectCross = null;
								List<ReltioObject> reltioObjects = null;

								List<String[]> outputLineValues = new ArrayList<>();

								List<ReltioObject> reltioObjectsOfallRecords = new ArrayList<>();

								for (String[] lineValues : threadInputLines) {
									String startObj = null;
									String endObj = null;
									String[] output;
									if (lineValues.length != sizeOfHeaderColumn) {
										failedRecordsWriter.writeToFile(JSONGeneratorUtil
												.addValueInArrayBegining(
														" Number of column values not matching with header column count:: Header count: "
																+ sizeOfHeaderColumn
																+ " .. Current Row column values count: "
																+ lineValues.length,
														lineValues));
										System.out.println("Rejected: Number of column values not matching with header column count:|"
												+ Arrays.toString(lineValues));
										rejectedRecordCount[0]++;
										continue;
									}

									reltioObject = new ReltioObject();
									reltioObjectCross = new Crosswalk();
									reltioObjects = new ArrayList<>();

									reltioObject.type = configProperties
											.getRelationType();

									if (startObjectCrosswalkIndex != null) {
										if (NormalizedJsonConversationHelper
												.checkNotNull(lineValues[startObjectCrosswalkIndex])) {
											startObj = lineValues[startObjectCrosswalkIndex]
													.trim();
											reltioObject
													.addRelationStartObjectCrosswalk(
															configProperties
																	.getStartObjSourceSystem(),
															startObj);
										}
									} else {
										if (NormalizedJsonConversationHelper
												.checkNotNull(lineValues[startObjectEntityUriIndex])) {
											try {
												startObj = lineValues[startObjectEntityUriIndex]
														.trim();
											} catch (Exception e) {
												e.printStackTrace();
											}
											reltioObject
													.addRelationEndObjectURI(startObj);

										}
									}

									if (endObjecCrosswalkIndex != null) {
										if (NormalizedJsonConversationHelper
												.checkNotNull(lineValues[endObjecCrosswalkIndex])) {
											try {
												endObj = lineValues[endObjecCrosswalkIndex]
														.trim();
											} catch (Exception e) {
												e.printStackTrace();
											}
											reltioObject
													.addRelationEndObjectCrosswalk(
															configProperties
																	.getEndObjSourceSystem(),
															endObj);
										}
									} else {
										if (NormalizedJsonConversationHelper
												.checkNotNull(lineValues[endObjectEntityUriIndex])) {
											endObj = lineValues[endObjectEntityUriIndex]
													.trim();
											reltioObject
													.addRelationEndObjectURI(endObj);

										}
									}

									if (startObj == null
											|| startObj.trim().isEmpty()) {
										failedRecordsWriter.writeToFile(JSONGeneratorUtil
												.addValueInArrayBegining(
														"Relation Start Object Crosswalk Column Empty",
														lineValues));
										rejectedRecordCount[0]++;
										continue;
									}

									if (endObj == null
											|| endObj.trim().isEmpty()) {
										failedRecordsWriter.writeToFile(JSONGeneratorUtil
												.addValueInArrayBegining(
														" Relation End Object Crosswalk Column Empty",
														lineValues));
										rejectedRecordCount[0]++;
										continue;
									}

									reltioObjectCross.type = configProperties
											.getSourceSystem();
									if (crosswalkValueColummnIndex != null
											&& lineValues[jsonGeneratorService.columnIndexMap.get(configProperties
													.getCrosswalkValueColumn())] != null) {
										reltioObjectCross.value = lineValues[jsonGeneratorService.columnIndexMap.get(configProperties
												.getCrosswalkValueColumn())]
												.trim();

									} else {
										reltioObjectCross.value = configProperties
												.getRelationType()
												.substring(
														configProperties
																.getRelationType()
																.lastIndexOf(
																		"/") + 1,
														configProperties
																.getRelationType()
																.length())
												+ "_"
												+ reltioObjectCross.type
														.substring(
																reltioObjectCross.type
																		.lastIndexOf("/") + 1,
																reltioObjectCross.type
																		.length())
												+ "_" + startObj + "_" + endObj;
									}
									reltioObject.addCrosswalks(
											configProperties.getSourceSystem(),
											reltioObjectCross.value.trim());

									if (attributesTemplateList != null
											&& !attributesTemplateList
													.isEmpty()) {
										try {
											reltioObject
													.createAttributes(
															attributesTemplateList,
															lineValues,
															jsonGeneratorService.columnIndexMap,
															jsonGeneratorService.nonColumnValues,
															crosswalk,
															configProperties
																	.getColumnDelimiter(),configProperties.isIgnoreNullAttribute(),
																	null);
										} catch (Exception e) {
											e.printStackTrace();
											failedRecordsWriter
													.writeToFile(JSONGeneratorUtil
															.addValueInArrayBegining(
																	" Generic Error: "
																			+ e.getMessage(),
																	lineValues));
											System.out.println("Rejected: Failed to do Attribute mapping|"
													+ Arrays.toString(lineValues));
											rejectedRecordCount[0]++;

										}
									}

									if (startDateIndex != null
											&& lineValues[startDateIndex] != null) {
										reltioObject
												.setStartDate(lineValues[startDateIndex]);
									}

									if (endDateIndex != null
											&& lineValues[endDateIndex] != null) {
										reltioObject
												.setEndDate(lineValues[endDateIndex]);
									}

									if (configProperties
											.getIsSingleJsonOutput()) {
										reltioObjectsOfallRecords
												.add(reltioObject);
									} else {
										reltioObjects.add(reltioObject);
										if (configProperties
												.getIsJsonWithOutCrosswalk()) {
											output = new String[1];
											output[0] = gson
													.toJson(reltioObjects);

										} else {
											output = new String[2];
											output[0] = reltioObjectCross.value;
											output[1] = gson
													.toJson(reltioObjects);
										}
										outputLineValues.add(output);
									}
								}
								if (!configProperties.getIsSingleJsonOutput()) {
									reltioFileWriter
											.writeToFile(outputLineValues);
								} else {
									jsonGeneratorService
											.addDataToReltioObjectList(reltioObjectsOfallRecords);
								}
								requestExecutionTime = System
										.currentTimeMillis() - startTime;
							} catch (Exception e) {
								e.printStackTrace();
								System.out
										.println("Unexpected Error happened .... "
												+ e.getMessage());
							}
							return requestExecutionTime;
						}
					}));

				}

				if (eof) {
					break;
				}

			}

			System.out.println("Number of records read from file =" + count);
			waitForTasksReady(futures, configProperties.getThreadCount()
					* (MAX_QUEUE_SIZE_MULTIPLICATOR / 2));
		}

		waitForTasksReady(futures, 0);

		if (configProperties.getIsSingleJsonOutput()) {
			reltioFileWriter.writeToFile(gson
					.toJson(jsonGeneratorService.reltioOjectsOfFile));
		}

		reltioFileWriter.close();
		inputDataFileReader.close();
		failedRecordsWriter.close();

		System.out
				.println("\n \n *** Final Metrics of the JSON Generation ***");
		System.out.println("Total Number of Records in the File = " + count);
		System.out.println("Total Number of Rejected Records = "
				+ rejectedRecordCount[0]);
		System.out.println("Total Number of records processed sucessfully = "
				+ (count - rejectedRecordCount[0]));
		System.out.println("Total Time Taken in (ms) = "
				+ (System.currentTimeMillis() - programStartTime));
		executorService.shutdown();
		if (rejectedRecordCount[0] == 0) {
			new File(failedRecordsFileName).delete();
		}
		System.out.println("Process Completed");
	}

	private synchronized void addDataToReltioObjectList(
			List<ReltioObject> objects) {
		reltioOjectsOfFile.addAll(objects);
	}
}
