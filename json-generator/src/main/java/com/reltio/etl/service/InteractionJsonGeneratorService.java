package com.reltio.etl.service;

import com.google.gson.Gson;
import com.reltio.cst.util.Util;
import com.reltio.etl.constants.JsonGeneratorProperties;
import com.reltio.etl.domain.Crosswalk;
import com.reltio.etl.domain.ReltioObject;
import com.reltio.etl.service.domain.InteractionConfigurationProperties;
import com.reltio.etl.service.util.JSONGeneratorUtil;
import com.reltio.etl.util.NormalizedJsonConversationHelper;
import com.reltio.file.ReltioCSVFileReader;
import com.reltio.file.ReltioCSVFileWriter;
import com.reltio.file.ReltioFileReader;
import com.reltio.file.ReltioFileWriter;
import com.reltio.file.ReltioFlatFileReader;
import com.reltio.file.ReltioFlatFileWriter;
import com.reltio.file.ReltioS3CSVFileReader;
import com.reltio.file.ReltioS3CSVFileWriter;
import com.reltio.file.ReltioS3FlatFileReader;
import com.reltio.file.ReltioS3FlatFileWriter;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import static com.reltio.etl.constants.JsonGeneratorCoreProps.STATIC_VALUE_PREFIX;
import static com.reltio.etl.constants.JsonGeneratorCoreProps.STATIC_VALUE_SUFFIX;
import static com.reltio.etl.constants.JsonGeneratorProperties.GROUP_TO_SENT;
import static com.reltio.etl.constants.JsonGeneratorProperties.MAX_QUEUE_SIZE_MULTIPLICATOR;
import static com.reltio.etl.service.util.JSONGeneratorUtil.printDataloadPerformance;
import static com.reltio.etl.service.util.JSONGeneratorUtil.waitForTasksReady;

/**
 * This is the main class for generating json for Entities. Handles both
 * Normalized and denormalized file columns
 */
public class InteractionJsonGeneratorService {
    private static final Logger logger = LogManager.getLogger(InteractionJsonGeneratorService.class.getName());
    private static Gson gson = new Gson();
    // Keep column name and index in the input file
    private final Map<String, Integer> columnIndexMap = new HashMap<>();
    // Keep non-valid column details list
    private final Set<String> nonColumnValues = new HashSet<>();
    // Keeps Normalized Value columns
    private final Set<String> normalizedColumns = new HashSet<>();
    private final List<ReltioObject> reltioOjectsOfFile = new ArrayList<>();
    // keeps the Source System of the File
    private String fileSourceSystem = null;

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static void main(String[] args) throws Exception {

        logger.info("Process Started");
        ReltioFileWriter tempReltioFileWriter = null;
        ReltioFileWriter tempfailedRecordsWriter = null;
        long programStartTime = System.currentTimeMillis();
        int count = 0;
        final InteractionJsonGeneratorService jsonGeneratorService = new InteractionJsonGeneratorService();
        final int[] rejectedRecordCount = new int[1];
        rejectedRecordCount[0] = 0;

        Properties properties = new Properties();
        FileReader fileReader = null;
        try {
            String propertyFilePath = args[0];
            fileReader = new FileReader(
                    StringEscapeUtils.escapeJava(propertyFilePath));
            //properties.load(fileReader);
            properties = Util.getProperties(propertyFilePath, "AWS_KEY", "AWS_SECRET_KEY");
        } catch (Exception e) {
            logger.error("Failed Read the Properties File :: ");
            logger.error(e.getMessage());
            logger.debug(e);
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    logger.error(e.getMessage());
                    logger.debug(e);
                }
            }

        }
        // READ the Config Properties values
        final InteractionConfigurationProperties configProperties = new InteractionConfigurationProperties(
                properties);

        List<String> requiredProps = Arrays.asList(
                "INPUT_DATA_FILE",
                "INTERACTION_TYPE",
                "INPUT_FILE_FORMAT",
                "OUTPUT_FILE",
                "SOURCE_SYSTEM",
                "MEMBERS_MAPPING_FILE",
                "ATTRIBUTE_MAPPING_FILE",
                "SOURCE_SYSTEM",
                "CROSSWALK_VALUE_COLUMN");

        List<String> missingProps = Util.listMissingProperties(properties, requiredProps);

        if (missingProps.size() > 0) {
            logger.error("Process Aborted due to insufficient input properties... Below are the list of missing properties");
            logger.error( String.join("\n", missingProps));
            System.exit(-1);
        }

        jsonGeneratorService.fileSourceSystem = configProperties
                .getSourceSystem();

        // Read the Mapping File configuration
        properties.clear();

        try {
            fileReader = new FileReader(
                    StringEscapeUtils.escapeJava(configProperties
                            .getAttributeMappingFilePath()));
            properties.load(fileReader);

        } catch (Exception e) {
            logger.error("Failed to Read the Properties File :: "
                    + configProperties.getAttributeMappingFilePath());
            logger.error(e.getMessage());
            logger.debug(e);
            System.exit(-1);
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    logger.error(e.getMessage());
                    logger.debug(e);
                }
            }
        }
        final Map<String, String> attributeMappingDetails = new HashMap<>(
                (Map) properties);

        // Read the Members Mapping File configuration
        properties.clear();

        try {
            fileReader = new FileReader(
                    StringEscapeUtils.escapeJava(configProperties
                            .getMemberseMappingFilePath()));
            properties.load(fileReader);

        } catch (Exception e) {
            logger.error("Failed to Read the Properties File :: "
                    + configProperties.getMemberseMappingFilePath());
            logger.error(e.getMessage());
            logger.debug(e);
            System.exit(-1);
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    logger.error(e.getMessage());
                    logger.debug(e);
                }
            }
        }
        final Map<String, String> membersMappingDetails = new HashMap<>(
                (Map) properties);

        // Create Reader for input data File
        ReltioFileReader inputDataFileReader = null;
        String failedRecordsFileName;
		
        if (configProperties.getInputDataFilePath().substring(0, 2).equalsIgnoreCase("S3")) {
			String awsKey  = configProperties.getAwsKey();
			String awsSecretKey  = configProperties.getAwsSecretKey();
			String awsRegion = configProperties.getAwsRegion();
			String bucketName = configProperties.getInputDataFilePath().substring(5, StringUtils.ordinalIndexOf(StringEscapeUtils.escapeJava(configProperties.getInputDataFilePath()), "/", 3));
			String fileName = configProperties.getInputDataFilePath().substring(StringUtils.ordinalIndexOf(StringEscapeUtils.escapeJava(configProperties.getInputDataFilePath()), "/", 3)+1);
		if (configProperties.getInputFileFormat().equals("CSV")) {
			inputDataFileReader = new ReltioS3CSVFileReader(awsKey, awsSecretKey, awsRegion, bucketName, fileName);
		} else {
			if (configProperties.getInputFileDelimiter() == null) {
				inputDataFileReader = new ReltioS3FlatFileReader(awsKey, awsSecretKey, awsRegion, bucketName, fileName);
			} else {
				inputDataFileReader = new ReltioS3FlatFileReader(awsKey, awsSecretKey, awsRegion, bucketName, fileName,
						configProperties.getInputFileDelimiter());	
			}
		}
		} else{
			if (configProperties.getInputFileFormat().equals("CSV")) {
				inputDataFileReader = new ReltioCSVFileReader(configProperties.getInputDataFilePath());
			} else {
				if (configProperties.getInputFileDelimiter() == null) {
					inputDataFileReader = new ReltioFlatFileReader(configProperties.getInputDataFilePath());
				} else {
					inputDataFileReader = new ReltioFlatFileReader(configProperties.getInputDataFilePath(),
							configProperties.getInputFileDelimiter());	
				}
			}
		}
        if (configProperties.getOutputFilePath().substring(0, 2).equalsIgnoreCase("S3")) {
			String awsKey  = configProperties.getAwsKey();
			String awsSecretKey  = configProperties.getAwsSecretKey();
			String awsRegion = configProperties.getAwsRegion();
			String outBucketName = configProperties.getOutputFilePath().substring(5, StringUtils.ordinalIndexOf(StringEscapeUtils.escapeJava(configProperties.getOutputFilePath()), "/", 3));
			String outFileName = configProperties.getOutputFilePath().substring(StringUtils.ordinalIndexOf(StringEscapeUtils.escapeJava(configProperties.getOutputFilePath()), "/", 3)+1);
			tempReltioFileWriter = new ReltioS3FlatFileWriter(outBucketName, outFileName, awsKey, awsSecretKey, awsRegion);
			if (configProperties.getInputFileFormat().equals("CSV")) {
				failedRecordsFileName = outFileName + "_RejectedRecords.csv";
				tempfailedRecordsWriter = new ReltioS3CSVFileWriter(outBucketName, failedRecordsFileName, awsKey, awsSecretKey, awsRegion);	
			}else {
				failedRecordsFileName = outFileName + "_RejectedRecords.txt";
				if (configProperties.getInputFileDelimiter() == null) {
					tempfailedRecordsWriter = new ReltioS3FlatFileWriter(outBucketName, failedRecordsFileName, awsKey, awsSecretKey, awsRegion);
				} else {
					tempfailedRecordsWriter = new ReltioS3FlatFileWriter(outBucketName, failedRecordsFileName, awsKey, awsSecretKey, awsRegion, "UTF-8",
							configProperties.getInputFileDelimiter());	
				}
			}
		} else {
			tempReltioFileWriter = new ReltioFlatFileWriter(configProperties.getOutputFilePath());
			if (configProperties.getInputFileFormat().equals("CSV")) {
				failedRecordsFileName = configProperties.getOutputFilePath() + "_RejectedRecords.csv";
				tempfailedRecordsWriter = new ReltioCSVFileWriter(failedRecordsFileName);
			}else {
				failedRecordsFileName = configProperties.getOutputFilePath() + "_RejectedRecords.txt";
				if (configProperties.getInputFileDelimiter() == null) {
					tempfailedRecordsWriter = new ReltioFlatFileWriter(failedRecordsFileName);
				} else {
					tempfailedRecordsWriter = new ReltioFlatFileWriter(failedRecordsFileName, "UTF-8",
							configProperties.getInputFileDelimiter());	
				}
			}
		}
        
        // Create Writter for outputFile
        final ReltioFileWriter reltioFileWriter = tempReltioFileWriter;
        final ReltioFileWriter failedRecordsWriter = tempfailedRecordsWriter;

        String[] lineValues = null;

        // Read Header
        lineValues = inputDataFileReader.readLine();
        failedRecordsWriter.writeToFile(JSONGeneratorUtil
                .addValueInArrayBegining(
                        JsonGeneratorProperties.FAILED_RECORD_REASON,
                        lineValues));

        final int sizeOfHeaderColumn = lineValues.length;

        List<String> allColumnNames = Arrays.asList(lineValues);

        // Create a Map for Column Name to Index
        String columnName = null;
        for (int i = 0; i < allColumnNames.size(); i++) {

            columnName = allColumnNames.get(i).trim();

            if (NormalizedJsonConversationHelper.isMultiValue(columnName)) {

            }
            jsonGeneratorService.columnIndexMap.put(allColumnNames.get(i)
                    .trim(), i);
        }

        // Populate all the attributes from mapping file to Reltio Dummy Object
        final Map<String, Object> attributesTemplateList = JsonGeneratorService
                .createReltioTemplateObjectAttributesList(
                        attributeMappingDetails,
                        jsonGeneratorService.columnIndexMap,
                        jsonGeneratorService.nonColumnValues,
                        null,
                        jsonGeneratorService.normalizedColumns);

        // Populate all the memebers from mapping file to Reltio Dummy Object
        final Map<String, Object> membersTemplateList = JsonGeneratorService
                .createReltioTemplateInteractionMemberList(
                        membersMappingDetails,
                        jsonGeneratorService.columnIndexMap,
                        jsonGeneratorService.nonColumnValues,
                        jsonGeneratorService.fileSourceSystem,
                        jsonGeneratorService.normalizedColumns);

        if ((configProperties.getColumnDelimiter() == null || configProperties
                .getColumnDelimiter().isEmpty())
                && !jsonGeneratorService.normalizedColumns.isEmpty()) {
            logger.error("Below File columns provided in the mapping File mentioned as Normalized Columns. But the NORMALIZED_FILE_COLUMN_DELIMITER property is empty..");

            for (String column : jsonGeneratorService.normalizedColumns) {
                logger.error(column);
            }

            logger.error("Please update the Job Configuration file with the NORMALIZED_FILE_COLUMN_DELIMITER value and Try again...");
            System.exit(-1);

        }

        // Check the crosswalk Colummn
        if (jsonGeneratorService.columnIndexMap.get(configProperties
                .getCrosswalkValueColumn()) == null) {
            jsonGeneratorService.nonColumnValues.add(configProperties
                    .getCrosswalkValueColumn());
        }

        boolean isNonColumnValuePresent = false;

        if (!jsonGeneratorService.nonColumnValues.isEmpty()) {

            for (String col : jsonGeneratorService.nonColumnValues) {
                if (!col.startsWith(STATIC_VALUE_PREFIX)
                        || !col.endsWith(STATIC_VALUE_SUFFIX)) {
                    if (!isNonColumnValuePresent) {
                        logger.error("Non-Column Values in the Mapping File Below::::");
                    }
                    logger.error(col);
                    isNonColumnValuePresent = true;
                }
            }
        }

        if (isNonColumnValuePresent) {
            logger.error("Please validate the Mapping File Column Names listed above and re-try.....");
            System.exit(-1);
        }

        final Integer timeStampColumnIndex;
        if (configProperties.getTimestampColumn() != null && !configProperties.getTimestampColumn().isEmpty()) {

            timeStampColumnIndex = jsonGeneratorService.columnIndexMap.get(configProperties.getTimestampColumn().trim());
        } else {
            timeStampColumnIndex = null;
        }

        // Thread Operations
        ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors
                .newFixedThreadPool(configProperties.getThreadCount());
        ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();
        boolean eof = false;

        // Create Reltio Dummy Object
        final Crosswalk crosswalk = new Crosswalk();
        crosswalk.type = configProperties.getSourceSystem();
        crosswalk.value = configProperties.getCrosswalkValueColumn();
        List<String[]> inputLineValues = new ArrayList<>();
        long totalFuturesExecutionTime = 0L;
        while (!eof) {
            for (int threadNum = 0; threadNum < configProperties
                    .getThreadCount() * MAX_QUEUE_SIZE_MULTIPLICATOR; threadNum++) {
                inputLineValues.clear();

                for (int k = 0; k < GROUP_TO_SENT; k++) {

                    // Read line
                    lineValues = inputDataFileReader.readLine();
                    if (lineValues == null) {
                        eof = true;
                        break;
                    }
                    count++;
                    inputLineValues.add(lineValues);

                }
                logger.info("Number of records read from file=" + count);
                if (!inputLineValues.isEmpty()) {
                    final List<String[]> threadInputLines = new ArrayList<>(
                            inputLineValues);

                    futures.add(executorService.submit(new Callable<Long>() {

                        @Override
                        public Long call() {
                            long requestExecutionTime = 0l;
                            long startTime = System.currentTimeMillis();
                            try {
                                ReltioObject reltioObject;
                                Crosswalk reltioObjectCross = null;
                                List<ReltioObject> reltioObjects = null;
                                String[] output;
                                List<String[]> outputLineValues = new ArrayList<>();
                                List<ReltioObject> reltioObjectsOfallRecords = new ArrayList<>();
                                for (String[] lineValues : threadInputLines) {

                                    if (lineValues.length != sizeOfHeaderColumn) {
                                        failedRecordsWriter.writeToFile(JSONGeneratorUtil
                                                .addValueInArrayBegining(
                                                        " Number of column values not matching with header column count:: Header count: "
                                                                + sizeOfHeaderColumn
                                                                + " .. Current Row column values count: "
                                                                + lineValues.length,
                                                        lineValues));
                                        logger.error("Rejected: Number of column values not matching with header column count:|"
                                                + Arrays.toString(lineValues));
                                        rejectedRecordCount[0]++;
                                        continue;
                                    }

                                    reltioObject = new ReltioObject();
                                    reltioObjectCross = new Crosswalk();
                                    reltioObjects = new ArrayList<>();

                                    reltioObject.type = configProperties
                                            .getEntityType();

                                    reltioObjectCross.type = configProperties
                                            .getSourceSystem();
                                    reltioObjectCross.value = lineValues[jsonGeneratorService.columnIndexMap
                                            .get(configProperties
                                                    .getCrosswalkValueColumn())];

                                    reltioObject.addCrosswalks(
                                            configProperties.getSourceSystem(),
                                            reltioObjectCross.value);

                                    if (reltioObjectCross.value == null
                                            || reltioObjectCross.value
                                            .isEmpty()) {
                                        failedRecordsWriter.writeToFile(JSONGeneratorUtil.addValueInArrayBegining(
                                                configProperties
                                                        .getCrosswalkValueColumn()
                                                        + " :: Crosswalk Column Empty",
                                                lineValues));
                                        logger.error("Rejected: Crosswalk Column Empty|"
                                                + Arrays.toString(lineValues));
                                        rejectedRecordCount[0]++;
                                        continue;
                                    }

                                    try {
                                        reltioObject
                                                .createAttributes(
                                                        attributesTemplateList,
                                                        lineValues,
                                                        jsonGeneratorService.columnIndexMap,
                                                        jsonGeneratorService.nonColumnValues,
                                                        crosswalk,
                                                        configProperties
                                                                .getColumnDelimiter(), configProperties.isIgnoreNullAttribute(), null);

                                        reltioObject
                                                .createMembers(
                                                        membersTemplateList,
                                                        lineValues,
                                                        jsonGeneratorService.columnIndexMap,
                                                        jsonGeneratorService.nonColumnValues,
                                                        crosswalk);

                                        if (timeStampColumnIndex != null) {
                                            reltioObject.setTimestamp(lineValues[timeStampColumnIndex]);

                                        }

                                        if (configProperties
                                                .getIsSingleJsonOutput()) {
                                            reltioObjectsOfallRecords
                                                    .add(reltioObject);
                                        } else {
                                            reltioObjects.add(reltioObject);
                                            if (configProperties
                                                    .getIsJsonWithOutCrosswalk()) {
                                                output = new String[1];
                                                output[0] = gson
                                                        .toJson(reltioObjects);

                                            } else {
                                                output = new String[2];
                                                output[0] = reltioObjectCross.value;
                                                output[1] = gson
                                                        .toJson(reltioObjects);
                                            }
                                            outputLineValues.add(output);
                                        }
                                    } catch (Exception e) {
                                        logger.error(e.getMessage());
                                        logger.debug(e);
                                        failedRecordsWriter.writeToFile(JSONGeneratorUtil.addValueInArrayBegining(
                                                " Generic Error: "
                                                        + e.getMessage(),
                                                lineValues));
                                        logger.error("Rejected: Failed to do Attribute mapping|"
                                                + Arrays.toString(lineValues));
                                        rejectedRecordCount[0]++;
                                    }
                                }
                                if (!configProperties.getIsSingleJsonOutput()) {
                                    reltioFileWriter
                                            .writeToFile(outputLineValues);
                                } else {
                                    jsonGeneratorService
                                            .addDataToReltioObjectList(reltioObjectsOfallRecords);
                                }
                                requestExecutionTime = System
                                        .currentTimeMillis() - startTime;
                            } catch (Exception e) {
                                logger.error("Unexpected Error happened .... "
                                        + e.getMessage());
                                logger.error(e.getMessage());
                                logger.debug(e);
                            }
                            return requestExecutionTime;
                        }
                    }));

                }

                if (eof) {
                    break;
                }

            }
            logger.info("Number of Records  readed from the File ="
                    + count);
            totalFuturesExecutionTime += waitForTasksReady(futures, configProperties.getThreadCount()
                    * (MAX_QUEUE_SIZE_MULTIPLICATOR / 2));
            printDataloadPerformance(executorService.getCompletedTaskCount() * GROUP_TO_SENT, totalFuturesExecutionTime, programStartTime, configProperties.getThreadCount());

        }

        totalFuturesExecutionTime += waitForTasksReady(futures, 0);
        printDataloadPerformance(executorService.getCompletedTaskCount() * GROUP_TO_SENT, totalFuturesExecutionTime, programStartTime, configProperties.getThreadCount());


        if (configProperties.getIsSingleJsonOutput()) {
			/*reltioFileWriter.writeToFile(gson
					.toJson(jsonGeneratorService.reltioOjectsOfFile));*/
            String json = gson
                    .toJson(jsonGeneratorService.reltioOjectsOfFile);
            reltioFileWriter.writeToFile(json.replaceAll("\"" + JsonGeneratorProperties.NULL_PLACEHOLDER_VALUE + "\"", "null"));

        }

        reltioFileWriter.close();
        inputDataFileReader.close();

        failedRecordsWriter.close();

        logger.info("\n \n *** Final Metrics of the JSON Generation ***");
        logger.info("Total Number of Records in the File = " + count);
        logger.info("Total Number of Rejected Records = " + rejectedRecordCount[0]);
        logger.info("Total Number of records processed sucessfully = " + (count - rejectedRecordCount[0]));
        logger.info("Total Time Taken in (ms) = " + (System.currentTimeMillis() - programStartTime));
        executorService.shutdown();
        if (rejectedRecordCount[0] == 0) {
            new File(failedRecordsFileName).delete();
        }
		logger.info("Process Completed");
    }

    private synchronized void addDataToReltioObjectList(
            List<ReltioObject> objects) {
        reltioOjectsOfFile.addAll(objects);
    }
}
