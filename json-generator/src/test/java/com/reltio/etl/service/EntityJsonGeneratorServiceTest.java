package com.reltio.etl.service;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import com.bazaarvoice.jolt.JsonUtil;
import com.bazaarvoice.jolt.JsonUtilImpl;

/**
 * Created by vignesh on 23/5/17.
 */
public class EntityJsonGeneratorServiceTest {
    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();
    private JsonUtil jsonUtil = new JsonUtilImpl();
    
    @Test
    public void entityJsonGenerationrefRelMultiValue() throws Exception {

        String[] filePath = {"src/test/resources/refRelationMultivalue/config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Object inputJson = jsonUtil.classpathToObject("/refRelationMultivalue/refRelMultiValue-output.json");
        Object expectedJson = jsonUtil.classpathToObject("/refRelationMultivalue/refRelMultiValue-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }

    @Test
    @Ignore
    public void entityJsonGenerationwithS3() throws Exception {

        String[] filePath = {"/reltio-util-json-generator/src/test/resources/AWS-S3-Read-Write/EntityDeepNestedRefS3-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        //Actual output would be written to S3 output file specified in the above config file.
        //Expected output is : /reltio-util-json-generator/src/test/resources/AWS-S3-Read-Write/Output-expected.json

    }
    
    @Test
    public void entityJsonGenerationWithSourceTable() throws Exception {

        String[] filePath = {"src/test/resources/entity/entityJsonGenerationWithSourceTable-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Object inputJson = jsonUtil.classpathToObject("/entity/entityJsonGenerationWithSourceTable-json-src1.json");
        Object expectedJson = jsonUtil.classpathToObject("/entity/entityJsonGenerationWithSourceTable-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }

    @Test
    public void entityJsonGenerationWithoutSourceTable() throws Exception {

        String[] filePath = {"src/test/resources/entity/entityJsonGenerationWithoutSourceTable-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(100);
        Object inputJson = jsonUtil.classpathToObject("/entity/entityJsonGenerationWithoutSourceTable-json-src1.json");
        Object expectedJson = jsonUtil.classpathToObject("/entity/entityJsonGenerationWithoutSourceTable-expected.json");
        Assert.assertEquals(inputJson, expectedJson);
    }

    @Ignore
    @Test
    public void simpleTestJsonGenerator() throws Exception {

        String[] filePath = {"/home/vignesh/Documents/projects/loreal/dataload/scripts/test/config.properties"};
        EntityJsonGeneratorService.main(filePath);

    }


    @Test
    public void entityJsonGenerationWithSpecialChar() throws Exception {

        String[] filePath = {"src/test/resources/entity/entityJsonGenerationWithSourceTable-config-2.properties"};
        EntityJsonGeneratorService.main(filePath);
        Object inputJson = jsonUtil.classpathToObject("/entity/entityJsonGenerationWithSpecialChar-json-src2.json");
        Object expectedJson = jsonUtil.classpathToObject("/entity/entityJsonGenerationWithSpecialChar-json-src2-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }

    @Test
    public void entityJsonGenerationWithSpecialChar2() throws Exception {
        String[] filePath = {"src/test/resources/specialCharacter/entityJsonGenerationWithSourceTable-config-2.properties"};
        EntityJsonGeneratorService.main(filePath);
        Object inputJson = jsonUtil.classpathToObject("/specialCharacter/entityJsonGenerationWithSpecialChar-json-src2.json");
        Object expectedJson = jsonUtil.classpathToObject("/specialCharacter/entityJsonGenerationWithSpecialChar-json-src2-expected.json");
        Assert.assertEquals(inputJson, expectedJson);
    }


    @Test
    public void entityJsonGenerationWithNull() throws Exception {

        String[] filePath = {"src/test/resources/entity/entityJsonGenerationWithNull-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Object inputJson = jsonUtil.classpathToObject("/entity/entityJsonGenerationWithNull.json");
        Object expectedJson = jsonUtil.classpathToObject("/entity/entityJsonGenerationWithNull-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }


    @Test
    public void entityJsonGenerationWithoutNull() throws Exception {

        String[] filePath = {"src/test/resources/entity/entityJsonGenerationWithoutNull-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Object inputJson = jsonUtil.classpathToObject("/entity/entityJsonGenerationWithoutNull.json");
        Object expectedJson = jsonUtil.classpathToObject("/entity/entityJsonGenerationWithoutNull-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }

    @Test
    public void entityJsonGenerationTestCase1() throws Exception {

        String[] filePath = {"src/test/resources/entity/entityJsonGenerationWithNull-testCase1-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Object inputJson = jsonUtil.classpathToObject("/entity/entityJsonGenerationWithNull-testCase1.json");
        Object expectedJson = jsonUtil.classpathToObject("/entity/entityJsonGenerationWithNull-testCase1-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }

    @Test
    @Ignore
    public void entityJsonGenerationTestWithNested() throws Exception {
        // TODO: 31/8/18 Null values are getting generated for nested attributes
        // and needs to be fixed.
        String[] filePath = {"src/test/resources/entity/entityJsonGeneration-nested-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Object inputJson = jsonUtil.classpathToObject("/entity/entityJsonGenerationWithNested.json");
        Object expectedJson = jsonUtil
                .classpathToObject("/entity/entityJsonGenerationForNested-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }

    @Test
    public void entityJsonGenerationDeepNestedTest() throws Exception {
        String[] filePath = {"src/test/resources/deepnested/output-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Object expectedJson = jsonUtil.classpathToObject("/deepnested/test-expected.json");
        Object inputJson = jsonUtil.classpathToObject("/deepnested/test.json");
        Assert.assertEquals(expectedJson, inputJson);

    }

    @Test
    public void entityJsonGenerationContributorProvider() throws Exception {
        String[] filePath = {"src/test/resources/contributor/output-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Object expectedJson = jsonUtil.classpathToObject("/contributor/multiContributorExpected.json");
        Object inputJson = jsonUtil.classpathToObject("/contributor/multiContributorActual.json");
        Assert.assertEquals(expectedJson, inputJson);

    }

    @Test
    public void entityJsonGenerationDeepNestedTest1() throws Exception {
        String[] filePath = {"src/test/resources/deepnested2/output-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Object expectedJson = jsonUtil.classpathToObject("/deepnested2/test-expected.json");
        Object inputJson = jsonUtil.classpathToObject("/deepnested2/test.json");
        Assert.assertEquals(expectedJson, inputJson);

    }

    @Test
    @Ignore
    public void entityJsonGenerationMissingPropertyFileError() throws Exception {
        exit.expectSystemExitWithStatus(-1);
        String[] filePath = {"src/test/resources/missingProp/output-config.properties"};
        EntityJsonGeneratorService.main(filePath);

    }


    @Test
    public void entityJsonGenerationDeepNestedRefTest1() throws Exception {
        String[] filePath = {"src/test/resources/ref/entityJsonGenerationDeepNestedRefTest1-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object expectedJson = jsonUtil.classpathToObject("/ref/entityJsonGenerationDeepNestedRefTest1-expected.json");
        Object inputJson = jsonUtil.classpathToObject("/ref/entityJsonGenerationDeepNestedRefTest1-output.json");
        Assert.assertEquals(expectedJson, inputJson);

    }


    @Test
    public void entityJsonGenerationDeepNestedRefTest2() throws Exception {
        String[] filePath = {"src/test/resources/deepnested3/entityJsonGenerationDeepNestedRefTest1-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object expectedJson = jsonUtil.classpathToObject("/deepnested3/entityJsonGenerationDeepNestedRefTest1-expected.json");
        Object inputJson = jsonUtil.classpathToObject("/deepnested3/entityJsonGenerationDeepNestedRefTest1-output.json");
        Assert.assertEquals(expectedJson, inputJson);

    }


    @Test
    public void entityJsonGenerationDeepNestedRefTest3() throws Exception {
        String[] filePath = {"src/test/resources/deepnestedmultivalue/entityJsonGenerationDeepNestedRefTest1-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object expectedJson = jsonUtil.classpathToObject("/deepnestedmultivalue/entityJsonGenerationDeepNestedRefTest1-expected.json");
        Object inputJson = jsonUtil.classpathToObject("/deepnestedmultivalue/entityJsonGenerationDeepNestedRefTest1-output.json");
        Assert.assertEquals(expectedJson, inputJson);

    }

    @Test
    public void entityJsonGenerationTestWithNestedNormalized() throws Exception {
        // TODO: 31/8/18 Null values are getting generated for nested attributes
        // and needs to be fixed.
        String[] filePath = {"src/test/resources/nestednormalized/nestednormalized-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object inputJson = jsonUtil.classpathToObject("/nestednormalized/nestednormalized-output.json");
        Object expectedJson = jsonUtil
                .classpathToObject("/nestednormalized/nestednormalized-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }

    @Test
    public void entityJsonGenerationTestWithNestedNormalizedMultivalue() throws Exception {

        String[] filePath = {"src/test/resources/nestednormalizedmultivalue/nestednormalized-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object inputJson = jsonUtil.classpathToObject("/nestednormalizedmultivalue/nestednormalized-output.json");
        Object expectedJson = jsonUtil
                .classpathToObject("/nestednormalizedmultivalue/nestednormalized-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }

    @Test
    public void entityJsonGenerationTestWithNestedNormalizedMultivalue1() throws Exception {

        String[] filePath = {"src/test/resources/nestednormalizedmultivalue1/nestednormalized-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object inputJson = jsonUtil.classpathToObject("/nestednormalizedmultivalue1/nestednormalized-output.json");
        Object expectedJson = jsonUtil
                .classpathToObject("/nestednormalizedmultivalue1/nestednormalized-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }


    @Test
    public void entityJsonGenerationTestWithNestedNormalizedMultivalue2() throws Exception {

        String[] filePath = {"src/test/resources/nestednormalizedmultivalue2/nestednormalized-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object inputJson = jsonUtil.classpathToObject("/nestednormalizedmultivalue2/nestednormalized-output.json");
        Object expectedJson = jsonUtil
                .classpathToObject("/nestednormalizedmultivalue2/nestednormalized-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }


    @Test
    public void entityJsonGenerationTestWithNestedNormalizedMultivalue3() throws Exception {

        String[] filePath = {"src/test/resources/nestednormalizedmultivalue3/nestednormalized-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object inputJson = jsonUtil.classpathToObject("/nestednormalizedmultivalue3/nestednormalized-output.json");
        Object expectedJson = jsonUtil
                .classpathToObject("/nestednormalizedmultivalue3/nestednormalized-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }

    @Test
    public void entityJsonGenerationTestReference() throws Exception {

        String[] filePath = {"src/test/resources/refRelation/refrelation-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object inputJson = jsonUtil.classpathToObject("/refRelation/refrelation-output.json");
        Object expectedJson = jsonUtil
                .classpathToObject("/refRelation/refrelation-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }

    @Test
    public void entityJsonGenerationTestReference1() throws Exception {

        String[] filePath = {"src/test/resources/refRelation1/refrelation-config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object inputJson = jsonUtil.classpathToObject("/refRelation1/refrelation-output.json");
        Object expectedJson = jsonUtil
                .classpathToObject("/refRelation1/refrelation-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }

    @Test
    @Ignore
    public void entityJsonGenerationNestedWithBlank() throws Exception {

        String[] filePath = {"src/test/resources/nested-blank/config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object inputJson = jsonUtil.classpathToObject("/nested-blank/output.json");
        Object expectedJson = jsonUtil.classpathToObject("/nested-blank/expected.json");
        Assert.assertEquals(inputJson, expectedJson);
    }
    
    @Test
    public void entityJsonGenerationTestReference3() throws Exception {

        String[] filePath = {"src/test/resources/refRelation3/Account_Entity_Json_Config.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object inputJson = jsonUtil.classpathToObject("/refRelation3/refrelation-output.json");
        Object expectedJson = jsonUtil
                .classpathToObject("/refRelation3/refrelation-expected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }  
    @Test
    public void entitysubnestednormalizedJsonGenerationfalseTest() throws Exception {

        String[] filePath = {"src/test/resources/subnestednormalizedattributesfalse/CODS_HCP_CSV_TO_JSON_CONFIG_generatenulls_false.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object inputJson = jsonUtil.classpathToObject("/subnestednormalizedattributesfalse/subnestednormalizedfalse.json");
        Object expectedJson = jsonUtil
                .classpathToObject("/subnestednormalizedattributesfalse/subnestednormalizedfalseexpected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }
   @Test
    public void entitysubnestednormalizedJsonGenerationtrueTest() throws Exception {

        String[] filePath = {"src/test/resources/subnestednormalizedattributestrue/CODS_HCP_CSV_TO_JSON_CONFIG_generatenulls_true.properties"};
        EntityJsonGeneratorService.main(filePath);
        Thread.sleep(10000);
        Object inputJson = jsonUtil.classpathToObject("/subnestednormalizedattributestrue/subnestednormalizedtrue.json");
        Object expectedJson = jsonUtil
                .classpathToObject("/subnestednormalizedattributestrue/subnestednormalizedtrueexpected.json");
        Assert.assertEquals(inputJson, expectedJson);

    }  
   @Test
   public void entityrefrelationtypeTest() throws Exception {

       String[] filePath = {"src/test/resources/refRelationtype/CODS_HCP_CSV_TO_JSON_CONFIG.properties"};
       EntityJsonGeneratorService.main(filePath);
       Thread.sleep(10000);
       Object inputJson = jsonUtil.classpathToObject("/refRelationtype/CODS_HCP_JSON.json");
       Object expectedJson = jsonUtil
               .classpathToObject("/refRelationtype/CODS_HCP_JSON_expected.json");
       Assert.assertEquals(inputJson, expectedJson);

   }
   
   @Test
   @Ignore
   public void entityCUST_3171Test() throws Exception {

       String[] filePath = {"src/test/resources/bug.CUST-3171/output-config.properties"};
       EntityJsonGeneratorService.main(filePath);
       Thread.sleep(10000);
       Object inputJson = jsonUtil.classpathToObject("/bug.CUST-3171/test.json");
       Object expectedJson = jsonUtil
               .classpathToObject("/bug.CUST-3171/test-expected.json");

       Assert.assertEquals(inputJson, expectedJson);

   }
}